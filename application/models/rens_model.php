<?php
  class Rens_model extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getParameter()
    {
        $q = $this->db->get('parameters')->result_array();
        return $q;
    }

    public function getDataset()
    {
        $q = $this->db->get('training_set');
        return $q->result_array();
    }

    public function getSupplier()
    {
        $q = $this->db->get('supplier');
        return $q->result_array();
    }

    public function insertParameter()
    {
        $parameter = $this->input->post('parameter');
        $values_a = $this->input->post('values_a');
        $values_b = $this->input->post('values_b');

        $query = $this->db->query("ALTER TABLE `training_set` ADD `$parameter` ENUM('$values_a', '$values_b') NULL AFTER `id`;");

    }

  }
?>
