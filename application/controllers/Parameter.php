<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parameter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('rens_model');
	}

	public function index()
	{
		$data['parameter'] = $this->rens_model->getParameter();
		$this->load->view('components/header');
		$this->load->view('parameter',$data);
		$this->load->view('components/footer');

	}

	public function insertParameter()
	{
		// echo 'wal'; die;
		$data = array(
			'parameter' => $this->input->post('parameter'),
			'values_a' => $this->input->post('values_a'),
			'values_b' => $this->input->post('values_b')
		);
		$q = $this->db->insert('paramaters_values', $data);
		// print_r($q); die;
		
		$this->rens_model->insertParameter();

		$data2 = array(
			'parameter' => $this->input->post('parameter'),
		);
		$q2 = $this->db->insert('parameters', $data2);

		

		redirect("/");

	}

}
