<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('rens_model');
	}

	public function index()
	{
		$data['supplier'] = $this->rens_model->getSupplier();

		$this->load->view('components/header');
		$this->load->view('supplier',$data);
		$this->load->view('components/footer');

	}


}
