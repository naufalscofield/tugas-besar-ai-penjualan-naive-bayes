<?php
// ini_set('memory_limit', '1024M');
// ini_set('max_execution_time', 300);
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
		$this->load->model('rens_model');
		$this->load->library('session');
	}

	public function index()
	{
		$data['parameter'] = $this->rens_model->getParameter();
	
		$this->load->view('components/header');
		$this->load->view('naive_bayes',$data);
		$this->load->view('components/footer');
	}

	public function forecast()
	{
		$inputan = $this->input->post();
		// print_r($inputan['week']); die;

		//inisiasi hasil input user
		$week = $this->input->post('week');
		$weather = $this->input->post('weather');
		$holiday = $this->input->post('holiday');
		$month = $this->input->post('month');

		//memasukan hasil input user ke array
		$input = [$week,$weather,$holiday,$month];

		//Total data dalam dataset
		$totalData = $this->db->query("SELECT COUNT(*) as total_data FROM training_set")->row();
		$totalData = $totalData->total_data;

		//Total data crowded dalam dataset
		$totalCrowded = $this->db->query("SELECT COUNT(*) as total_crowded FROM training_set where sales = 'crowded'")->row();
		$totalCrowded = $totalCrowded->total_crowded;

		//Total data quiet dalam dataset
		$totalQuiet = $this->db->query("SELECT COUNT(*) as total_quiet FROM training_set where sales = 'quiet'")->row();
		$totalQuiet = $totalQuiet->total_quiet;
		
		//Prior Crowded
		$priorCrowded = $totalCrowded / $totalData;
		$priorCrowded = number_format((float)$priorCrowded, 2, '.', '');
		
		//Prior Quiet
		$priorQuiet = $totalQuiet / $totalData;
		$priorQuiet = number_format((float)$priorQuiet, 2, '.', '');

		//inisiasi array untuk menampung semua evidence crowded dan quiet berdasarkan hasil inputan user
		$tempEvidenceCrowded = [];
		$tempEvidenceQuiet = [];

		//insiaisi variable total evidence crowded dan quiet untuk dikalikan nantinya saat while
		$likelihoodCrowded = 1;
		$likelihoodQuiet = 1;

		$i = 0;
		// EVIDENCE
		$field = $this->db->list_fields('training_set');
		foreach ($field as $field) {
			if ($field == 'id' || $field == 'sales') {
				continue;
			}else{
				$eviCrowded = $this->db->query("SELECT count(`".$field."`) as crowded FROM training_set WHERE `".$field."` = '".$input[$i]."' AND sales = 'crowded'")->row();
				foreach ($eviCrowded as $eviCrowded) {
					// print_r($eviCrowded); die;
					$crowdedMember = $eviCrowded / $totalCrowded;
					$crowdedMember = number_format((float)$crowdedMember, 2, '.', '');
					array_push($tempEvidenceCrowded, $crowdedMember ) ;
				}
				// print_r($tempEvidenceCrowded); die;
				$eviQuiet = $this->db->query("SELECT count(`".$field."`) as quiet FROM training_set WHERE `".$field."` = '".$input[$i]."' AND sales = 'quiet'")->row();
				foreach ($eviQuiet as $eviQuiet) { 
					// print_r($eviQuiet); die;
					$quietMember = $eviQuiet / $totalQuiet;
					$quietMember = number_format((float)$quietMember, 2, '.', '');
					array_push($tempEvidenceQuiet, $quietMember);
					
					$i++;
				};
			}
			// $tes = count($tempEvidenceCrowded);
			// print_r($tes); die;
			
		}
				for ($j=0; $j < count($tempEvidenceCrowded); $j++) {
					$likelihoodCrowded = $likelihoodCrowded * $tempEvidenceCrowded[$j];
				}
	
				for ($j=0; $j < count($tempEvidenceQuiet); $j++) {
					$likelihoodQuiet = $likelihoodQuiet * $tempEvidenceQuiet[$j];
				}

				$probabilityCrowded = $likelihoodCrowded * $priorCrowded;
				$probabilityQuiet = $likelihoodQuiet * $priorQuiet;

				if ($probabilityCrowded < $probabilityQuiet) {
					// $data['sales'] = "QUIET";
					// $data['status'] = "quiet";
					// $data['prior'] = $priorQuiet;
					// $data['likelihood'] = $likelihoodQuiet;
					// $data['probability_ok'] = $likelihoodQuiet;
					// $data['probability_no'] = $likelihoodCrowded;
					// $data['message'] = $this->db->get('supplier')->result_array();

					$sales = "QUIET";
					$status = "quiet";
					$prior = $priorQuiet;
					$likelihood = $likelihoodQuiet;
					$probability_ok = $probabilityQuiet;
					$probability_no = $probabilityCrowded;
					$message = $this->db->get('supplier')->result_array();

					$session = array(
						'sales'  => $sales,
						'status'     => $status,
						'prior' => $prior,
						'likelihood' => $likelihood,
						'probability_ok' => $probability_ok,
						'probability_no' => $probability_no,
						'message' => $message
					);
				
					$this->session->set_userdata($session);
					redirect(base_url()."index.php/welcome/result");

				} else {
					$sales = "CROWDED";
					$status = "crowded";
					$prior = $priorCrowded;
					$likelihood = $likelihoodCrowded;
					$probability_ok = $probabilityCrowded;
					$probability_no = $probabilityQuiet;
					$message = $this->db->get('supplier')->result_array();

					$session = array(
						'sales'  => $sales,
						'status'     => $status,
						'prior' => $prior,
						'likelihood' => $likelihood,
						'probability_ok' => $probability_ok,
						'probability_no' => $probability_no,
						'message' => $message
					);
				
					$this->session->set_userdata($session);
					redirect(base_url()."index.php/welcome/result");
				}

	}

	public function result()
	{
		// print_r($this->session->userdata('sales')); die;
		$this->load->view('components/header');
		$this->load->view('result');
		$this->load->view('components/footer');
	}
}
