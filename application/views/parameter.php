<div class="sidebar" data-background-color="white" data-active-color="danger">
<?php
// print_r($field); die; ?>
    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <center><img src="<?= base_url();?>/img/logo.png" style="width:200px;height:50px" alt=""></center>
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="<?= base_url();?>/">
                        <i class="fa fa-tachometer"></i>
                        <p>Naive Bayes</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>index.php/Dataset">
                        <i class="fa fa-th"></i>
                        <p>Dataset</p>
                    </a>
                </li>
                <li class="active">
                    <a href="<?= base_url();?>index.php/parameter">
                        <i class="fa fa-tasks"></i>
                        <p>Parameter</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>index.php/supplier">
                        <i class="fa fa-shopping-cart"></i>
                        <p>Supplier</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url();?>decision">
                        <i class="fa fa-question"></i>
                        <p>Decision</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

<div class="main-panel">
		<nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar bar1"></span>
                        <span class="icon-bar bar2"></span>
                        <span class="icon-bar bar3"></span>
                    </button>
                    <a class="navbar-brand">Parameter</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
						<li>
                            <!-- <a href="/logout">
								<i class="fa fa-sign-out"></i>
								<p>Logout</p>
                            </a> -->
                        </li>
                    </ul>

                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><button class="btn btn-success btn-fill" type="button" id="btn_input" data-toggle="modal" data-target="#tambahBarang"><i class="fa fa-plus"></i></button></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="" class="table table-striped">
                                    <thead>
                                        <tr>
                                        <?php
                                        foreach ($parameter as $parameter) { 
                                            ?>
                                        <th width="20"><center><b><?= $parameter['parameter'];?></b></center></th>
                                            <?php } ?>
                                        <!-- <th width="20"><center><b>Action</b></center></th> -->
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                    <tr>
                                    <?php
                                    $p = $this->db->get('parameters')->result_array();
                                  
                                     foreach ($p as $p) {
                                  
                                         $values = $this->db->get_where('paramaters_values', array('parameter' => $p['parameter']))->row(); 
                                             ?>
                                            <td width="20"><center><b><?= $values->values_a;?>,<?= $values->values_b;?></b></center></td>
                                        <?php  } ?>
                                        </tr>
                                </tbody>    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="tambahBarang" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Add Parameter</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="add" action="<?= base_url();?>index.php/parameter/insertParameter" method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Parameter Name</label>
                                        <input type="text" required id="parameter" class="form-control border-input" placeholder="Parameter Name" value="" name="parameter">
                                    </div>
                                </div>
                            </div>

        
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Values A</label>
                                        <input type="text" required id="values_a" class="form-control border-input" placeholder="Values A" value="" name="values_a">
                                    
                                    </div>
                                </div>
                           
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Values B</label>
                                        <input type="text" required id="values_b" class="form-control border-input" placeholder="Values B" value="" name="values_b">
                                    
                                    </div>
                                </div>
                            
                            </div>    
                        
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button id="btn_add" type="submit" class="btn btn-success btn-fill btn-wd">
                                    Add New Parameter
                                </button>
                                <button type="submit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div id="editDataset" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                         <div class="modal-header">
                            <h5 class="modal-title">Edit Barang</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div class="modal-body">
                        <form id="update" method="post" action="<?= base_url();?>welcome/updateBarang">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ID</label>
                                        <input type="hidden" required id="input_id" class="form-control border-input" placeholder="Nama Barang" value="" name="id">
                                    </div>
                                </div>
                            </div>  

                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama Barang</label>
                                        <input type="text" required id="input_nama_barang" class="form-control border-input" placeholder="Nama Barang" value="" name="nama_barang">
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Pilih Lorong</label>
                                        <select  required name="id_lorong" class="form-control border-input" id="input_select_lorong">
                                            <option value="">--Pilih Lorong--</option>
                                            <?php 
                                                foreach ($lorong as $key) { ?>
                                            <option value="<?= $key['id'];?>"><?= $key['nama_lorong'];?></option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Pilih Rak</label>
                                        <select  required name="id_rak" class="form-control border-input" id="input_select_rak">
                                            <option value="">-- --</option>
                                        </select>
                                    </div>
                                </div>
                            </div>  
                            <div class="clearfix"></div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-info btn-fill btn-wd">
                                    Edit Barang
                                </button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url ()?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
$(document).ready( function () {
    // $('#myTable').DataTable();

    $('#myTable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
                ],
            } );

     $('#select_lorong').on('change', function() {
        // jika kelas dirubah
        console.log('aw');
        $.ajax({
        type: "POST",
        data: { id_lorong: $('#select_lorong').val() },
        url: '<?php echo base_url()."welcome/getRak" ?>',
        dataType: 'text',
        success: function(resp) {
            console.log('respon',resp)
            var json = JSON.parse(resp.replace(',.', ''))
            var $el = $("#select_rak");
            $el.empty(); // remove old options
            $el.append($("<option></option>")
            .attr("value", '').text('-- Pilih Rak --'));
            $.each(json, function(key, value) {
            $el.append($("<option></option>")
            .attr("value", value.id).text(value.nama_rak));
            });
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR, exception)
            }
        });
    });

     $('#input_select_lorong').on('change', function() {
        // jika kelas dirubah
        console.log('aw');
        $.ajax({
        type: "POST",
        data: { id_lorong: $('#input_select_lorong').val() },
        url: '<?php echo base_url()."welcome/getRak" ?>',
        dataType: 'text',
        success: function(resp) {
            console.log('respon',resp)
            var json = JSON.parse(resp.replace(',.', ''))
            var $el = $("#input_select_rak");
            $el.empty(); // remove old options
            $el.append($("<option></option>")
            .attr("value", '').text('-- Pilih Rak --'));
            $.each(json, function(key, value) {
            $el.append($("<option></option>")
            .attr("value", value.id).text(value.nama_rak));
            });
        },
        error: function (jqXHR, exception) {
            console.log(jqXHR, exception)
            }
        });
    });

    $(document).on('click', '#btn_edit' ,function(){
        // console.log($(this).data("nama_barang_edit"));
        var id = $(this).data("id_edit");
        var nama_barang = $(this).data("nama_barang_edit")
        console.log(id,nama_barang)
        $('#input_id').val(id)
        $('#input_nama_barang').val(nama_barang)
            });

} );
</script>
@endsection