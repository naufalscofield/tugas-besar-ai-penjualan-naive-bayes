-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2019 at 01:43 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rens_naivebayes`
--

-- --------------------------------------------------------

--
-- Table structure for table `paramaters_values`
--

CREATE TABLE `paramaters_values` (
  `id` int(11) NOT NULL,
  `parameter` varchar(20) NOT NULL,
  `values_a` varchar(20) NOT NULL,
  `values_b` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paramaters_values`
--

INSERT INTO `paramaters_values` (`id`, `parameter`, `values_a`, `values_b`) VALUES
(1, 'week', 'weekend', 'weekday'),
(2, 'weather', 'sunny', 'rainy'),
(3, 'holiday', 'yes', 'no'),
(4, 'month', 'early', 'end'),
(5, 'Diskon', 'yes', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` int(11) NOT NULL,
  `parameter` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `parameter`) VALUES
(1, 'week'),
(2, 'weather'),
(3, 'holiday'),
(4, 'month'),
(5, 'Diskon');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `product` varchar(50) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `product_units` varchar(10) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `crowded` int(11) NOT NULL,
  `quiet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id`, `product`, `full_name`, `product_units`, `phone`, `crowded`, `quiet`) VALUES
(1, 'plastik', 'Udin Zaenudin', 'bungkus', '089873819090', 10, 5),
(2, 'bubuk coklat', 'Rehan Siregar', 'bungkus', '087612345678', 50, 20);

-- --------------------------------------------------------

--
-- Table structure for table `training_set`
--

CREATE TABLE `training_set` (
  `id` int(11) NOT NULL,
  `Diskon` enum('yes','no') DEFAULT NULL,
  `week` enum('weekday','weekend') NOT NULL,
  `weather` enum('rainy','sunny') NOT NULL,
  `holiday` enum('yes','no') NOT NULL,
  `month` enum('early','end') NOT NULL,
  `sales` enum('crowded','quiet') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_set`
--

INSERT INTO `training_set` (`id`, `Diskon`, `week`, `weather`, `holiday`, `month`, `sales`) VALUES
(1, NULL, 'weekend', 'sunny', 'no', 'early', 'crowded'),
(2, NULL, 'weekend', 'rainy', 'no', 'end', 'quiet'),
(3, NULL, 'weekday', 'sunny', 'no', 'early', 'crowded'),
(4, NULL, 'weekday', 'sunny', 'yes', 'end', 'quiet'),
(5, NULL, 'weekend', 'rainy', 'yes', 'early', 'quiet'),
(6, NULL, 'weekday', 'rainy', 'no', 'early', 'quiet'),
(7, NULL, 'weekday', 'sunny', 'no', 'end', 'crowded'),
(8, NULL, 'weekend', 'sunny', 'yes', 'end', 'crowded'),
(9, NULL, 'weekend', 'rainy', 'no', 'early', 'quiet'),
(10, NULL, 'weekday', 'sunny', 'no', 'early', 'quiet'),
(11, NULL, 'weekday', 'sunny', 'yes', 'end', 'crowded'),
(12, NULL, 'weekend', 'rainy', 'no', 'end', 'crowded'),
(13, NULL, 'weekend', 'sunny', 'yes', 'early', 'quiet'),
(14, NULL, 'weekday', 'rainy', 'yes', 'end', 'quiet'),
(15, NULL, 'weekend', 'rainy', 'yes', 'end', 'crowded'),
(16, NULL, 'weekend', 'rainy', 'yes', 'end', 'quiet');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `paramaters_values`
--
ALTER TABLE `paramaters_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_set`
--
ALTER TABLE `training_set`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `paramaters_values`
--
ALTER TABLE `paramaters_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `training_set`
--
ALTER TABLE `training_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
